﻿// using System;


using System.Diagnostics;
using System.Runtime.Intrinsics.Arm;


interface IMouse
{
    bool rightClick();
    bool leftClick();
}

interface IKeyboard
{
    char getPressedSymb();
}
interface IControls: IKeyboard, IMouse
{
    void on();
    void off();
}

abstract class Monitor
{
    public abstract void show();
}

class MyPC: Monitor, IControls
{
    public void on(){}
    public void off(){}
    public char getPressedSymb(){return 'c'; }
    public bool leftClick(){return true; }
    public bool rightClick(){return true; }
    public override void show(){}
    
}

class SomeClass
{
    protected int x;
}
class Another: SomeClass
{
    public void func()
    {
    
    }
}



public class Human
{
    int age;
    string name;
    public Human() {}
    public Human(int age)
    :this("", age) {}
    public Human(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public void print()
    {
        Console.WriteLine("Name: {0}", name);
        Console.WriteLine("Age: {0}", age);
    }

}

public class Student: Human
{
    static int counts;
    string university;
    public Student() {}
    public Student(int age): base(age)
    {
        Console.WriteLine("Dynamic");
    }
    static Student()
    { 
        counts = 0;
        Console.WriteLine("Static");
    }
    public Student(string name, int age): base(name, age){}
    public Student(string name, int age, string university): base(name, age)
    {
        this.university = university;
    }
    public void print()
    {
        base.print();
        Console.WriteLine("University: {0}", university);
    }
    public void fRef(ref int i)
    {
        i=10;
    }
    public void fOut(out int i)
    {
        i = 5;
    }
    
    public void func()
    {
        int i = 3;
    
        object o = i; //boxing
        
        o = 5;
        int j = (int)o; //unboxing
    
        object implicitO = i;
        object expliсitO = (object)i;
    
        Console.WriteLine("i={0}; j={1}", i, j);
        
        fRef(ref i);
        fOut(out j);
        Console.WriteLine("After using ref, out i={0}; j={1}", i, j);
    
    }
}

class MyClass
{
    private class Nested
    {
        public void foo() => Console.WriteLine("Nested class");
    }
}

public enum Days
{
    None,  // 0
    Monday,  // 1
    Tuesday,  // 2
    Wednesday,  // 4
    Thursday,  // 8
    Friday,  // 16
    Saturday,  // 32
    Sunday  
}


class c1
{
    public c1()
    {
        Console.WriteLine("Dynamic");
    }

    static c1()
    {
        Console.WriteLine("Static");
    }
}


struct test
{
    private int i;
}
struct MyStruct
{
    public Stopwatch sw;   
    public class c
    {
        public void publicMethod()
        {
            // Console.WriteLine("Public");
            int i = 59 + 231 / 42 * 124;
            
            // test t = new test();
        }
        protected void protectedMethod()
        {
            // Console.WriteLine("Protected");
            int i = 59 + 231 / 42 * 124;
            // test t = new test();
        }
        private void privateMethod()
        {
            // Console.WriteLine("Private");
            int i = 59 + 231 / 42 * 124;
            // test t = new test();
        }

        public void test()
        {
            
        }
    };

    public void test()
    {
        c test = new c();
        
        Stopwatch p1 = Stopwatch.StartNew();
        for (int i = 0; i < 1000000000; ++i)
        {
            test.publicMethod();
        }
        p1.Stop();
        
        
        Stopwatch p2 = Stopwatch.StartNew();
        for (int i = 0; i < 1000000000; ++i)
        {
            test.protectedMethod();
        }
        p2.Stop();
        
        
        Stopwatch p3 = Stopwatch.StartNew();
        for (int i = 0; i < 1000000000; ++i)
        {
            test.privateMethod();
        }
        p3.Stop();
        
        Console.WriteLine("time: {0}", p1.Elapsed);
        Console.WriteLine("time: {0}", p2.Elapsed);
        Console.WriteLine("time: {0}", p3.Elapsed);
        // c test = new c();
        // sw = Stopwatch.StartNew();
        // test.publicMethod();
        // sw.Stop();
        // Console.WriteLine("time: {0}", sw.Elapsed);
        //
        // sw = Stopwatch.StartNew();
        // test.protectedMethod();
        // sw.Stop();
        // Console.WriteLine("time: {0}", sw.Elapsed);
        //
        // sw = Stopwatch.StartNew();
        // test.privateMethod();
        // sw.Stop();
        // Console.WriteLine("time: {0}", sw.Elapsed);
    }
}

public class NewObject
{
    private int n;

    public NewObject(int val) => n = val;

    public int Value
    {
        get { return n; }
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || !(obj is NewObject))
            return false;
        else
            return n == ((NewObject)obj).n;
    }

    public override int GetHashCode()
    {
        return n.GetHashCode();
    }

    public NewObject Copy()
    {
        return (NewObject)this.MemberwiseClone();
    }

    public static bool ReferenceEquals(object? a, object? b)
    {
        return a.Equals(b);
    }

    public override string ToString()
    {
        return n.ToString();
    }
}


namespace prog
{
    class Program
    {
        
        static void Main(string[] args)
        {
            MyStruct s = new MyStruct();
            s.test();

            // NewObject o1 = new NewObject(8);
            // NewObject o2 = new NewObject(8);
            //
            // Console.WriteLine(NewObject.ReferenceEquals(o1, o2));
            // MyStruct s;
            // s.i = 5;
            // Console.WriteLine(s.i);


            // c1 c = new c1();

            // MyClass c = new MyClass();
            // c.
            // Student s = new Student();
            // Human h = s;
            // h.print();
            // s.func();


            // Console.WriteLine(Convert.ToInt32(Days.Thursday | Days.Friday));
            // Console.WriteLine(Convert.ToInt32(Days.Monday & Days.Saturday));
            // Console.WriteLine(Convert.ToInt32(Days.Monday ^ Days.Sunday));
            // Console.WriteLine(Days.Thursday == Days.Monday || Days.Friday == Days.Monday);
            // Console.WriteLine(Days.Monday == Days.Monday && Days.Saturday == Days.Saturday);
        }
        
    }
}
