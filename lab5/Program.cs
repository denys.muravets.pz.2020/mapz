﻿// See https://aka.ms/new-console-template for more information

using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;

namespace game
{
    class Tackle
    {
        public string Name { get; set; }
        // public int ID { get; set; }
        public string Description { get; set; }
    }

    class Inventory
    {
        public List<Tackle> tackles { get; set; }

        public Inventory()
        {
            tackles = new List<Tackle>();
        }

        public string ToString()
        {
            string str = "";
            foreach (var item in tackles)
            {
                str += item.Name + "\n";
            }

            return str;
        }
        // public void addTackle(Tackle tackle)
        // {
        //     this.tackles.Add(tackle);
        // }
        //
        // public Tackle getTackleByIndex(int index)
        // {
        //     return tackles[index];
        // }
        //
        // public void removeTackle(Tackle tackle)
        // {
        //     tackles.Remove(tackle);
        // }
        //
        // public void removeTackleAt(int index)
        // {
        //     tackles.RemoveAt(index);
        // }
    }

    class Nature
    {
        public static int Low;
        public static int Normal;
        public static int High;

        static Nature()
        {
            Low = 1;
            Normal = 2;
            High = 3;
        }
        public int id { get; set; }
        public int speed { get; set; }
        public int attentiveness { get; set; }
        public int savvy { get; set; }

        public int summ()
        {
            return speed + savvy + attentiveness;
        }
    }

    class Natures
    {
        public List<Nature> natures
        {
            get
            {
                return new List<Nature>()
                {
                    new Nature()
                    {
                        id = 0,
                        speed = Nature.Low,
                        attentiveness = Nature.Low,
                        savvy = Nature.Low

                    },
                    
                    new Nature()
                    {
                        id = 1,
                        speed = Nature.Low,
                        attentiveness = Nature.Normal,
                        savvy = Nature.High

                    },
                    
                    new Nature()
                    {
                        id = 2,
                        speed = Nature.Normal,
                        attentiveness = Nature.Low,
                        savvy = Nature.Normal

                    }
                    
                    
                };
            }
        }

        public static int getStartID()
        {
            return 0;
        }
    }


    class Person
    {
        public string Name { set; get; }
        public int NatureID { get; set; }
        public Inventory inventory;
        private static Person gamePerson = null;
        
        private Person(string name, int natureId)
        {
            this.Name = name;
            this.NatureID = natureId;
            this.inventory = new Inventory();
        }

        public static Person getInstance()
        {
            if (gamePerson == null)
                gamePerson = new Person("noname", Natures.getStartID());
            return gamePerson;
        }
    }

    class Map
    {
        public string description { get; private set; }

        public Map(string description)
        {
            this.description = description;
        }
    }

    class Image
    {
        private string src;

        public Image(string src)
        {
            this.src = src;
        }
       
    }
    
    // легковажник
    class UnitImagesFactory
    {
        public static Dictionary<string, Image> Images = new Dictionary<string, Image>();

        public static Image createFishImage(string fishName)
        {
            if (!Images.ContainsKey(fishName))
            {
                Images.Add(fishName, new Image(fishName+".png"));
            }

            return Images[fishName];
        }
    }
    
    class Fish
    {
        public string name { get; private set; }

        public Image image;
        // public string description { get; private set; }

        public Fish()
        {
            
        }
        public Fish(string name)
        {
            this.name = name;
            image = UnitImagesFactory.createFishImage(name);
            // this.description = description;
        }

        public virtual void eat()
        {
            Console.WriteLine("Eating...");
        }
    }

    
    // декоратор
    class DecoratorFish : Fish
    {
        protected Fish DecoratedFish { get; set; }
        public DecoratorFish(Fish decoratedFish)
        {
            DecoratedFish = decoratedFish;
        }

        public override void eat()
        {
            DecoratedFish.eat();
        }
        
    }

    class FishHut : DecoratorFish
    {
        public FishHut(Fish decoratedFish)
            : base(decoratedFish)
        {
            
        }

        public override void eat()
        {
            base.eat();
        }
        
        public void eatAnotherFish(Fish victim)
        {
            Console.WriteLine($"Fish {victim.name} is eaten!");
        }
    }
    
    
    
    abstract class InvetoryBuilder
    {
        protected Inventory inventory { get; private set; }

        public void createNewInventory()
        {
            inventory = new Inventory();
        }

        public abstract Inventory getInventorySet();
    }

    class StartingSetInventory : InvetoryBuilder
    {
        public override Inventory getInventorySet()
        {
            createNewInventory();
            inventory.tackles = new List<Tackle>()
            {
                new Tackle()
                {
                    Name = "fishing rod"
                },
                new Tackle()
                {
                    Name = "coil"
                },
                new Tackle()
                {
                    Name = "fishing line"
                },
                new Tackle()
                {
                    Name = "float"
                },
                new Tackle()
                {
                    Name = "hook"
                }
            };
            return inventory;
        }
    }

    class ExtentedSetInventory : InvetoryBuilder
    {
        public override Inventory getInventorySet()
        {
            createNewInventory();
            inventory.tackles = new List<Tackle>()
            {
                new Tackle()
                {
                    Name = "fishing rod"
                },
                new Tackle()
                {
                    Name = "coil"
                },
                new Tackle()
                {
                    Name = "fishing line"
                },
                new Tackle()
                {
                    Name = "float"
                },
                new Tackle()
                {
                    Name = "hook"
                },
                new Tackle()
                {
                    Name = "lure"
                },
                new Tackle()
                {
                    Name = "leash for a predator"
                },
                new Tackle()
                {
                    Name = "trough"
                }
            };
            return inventory;
        }
    }
    
    interface IFishingType
    {
        string getTypeName();
    }

    class ClassicFishing : IFishingType
    {
        public string getTypeName()
        {
            return "Classic fishing.";
        }
    }
    
    class FishingOnABoat : IFishingType
    {
        public string getTypeName()
        {
            return "Fishing on a boat.";
        }
    }
    
    interface IWeatherType
    {
        string getWeatherDescription();
    }

    class Sunny : IWeatherType
    {
        public string getWeatherDescription()
        {
            return "Sunny today!";
        }
    }

    class Gloomy : IWeatherType
    {
        public string getWeatherDescription()
        {
            return "Gloomy today!";
        }
    }

    class Rain : IWeatherType
    {
        public string getWeatherDescription()
        {
            return "Rain today!";
        }
    }
    
    class Shower : IWeatherType
    {
        public string getWeatherDescription()
        {
            return "Shower today!";
        }
    }
    
    interface ILocation
    {
        Map getMap();
        List<Fish> getFishList();
        List<Tackle> getTackleCatalog();
        IFishingType getFishingType();
        IWeatherType getWeatherDescription();
    }

    class RuralPond : ILocation
    {
        public Map getMap()
        {
            return new Map("A small pond on the outskirts of the village.");
        }

        public List<Fish> getFishList()
        {
            return new List<Fish>
            {
                new Fish("Crucian"),
                new Fish("Pike"),
                new Fish("Perch"),
                new Fish("Roach")
            };
        }

        public List<Tackle> getTackleCatalog()
        {
            InvetoryBuilder inventory = new StartingSetInventory();
            return inventory.getInventorySet().tackles;
        }

        public IFishingType getFishingType()
        {
            return new FishingOnABoat();
        }

        public IWeatherType getWeatherDescription()
        {
            return new Gloomy();
        }
    }
    
    class RiverBank : ILocation
    {
        public Map getMap()
        {
            return new Map("The bank of a large river.");
        }

        public List<Fish> getFishList()
        {
            return new List<Fish>
            {
                new Fish("Zander"),
                new Fish("Pike"),
                new Fish("Bream"),
                new Fish("Catfish")
            };
        }

        public List<Tackle> getTackleCatalog()
        {
            InvetoryBuilder inventory = new ExtentedSetInventory();
            return inventory.getInventorySet().tackles;
        }

        public IFishingType getFishingType()
        {
            return new ClassicFishing();
        }

        public IWeatherType getWeatherDescription()
        {
            return new Sunny();
        }
    }

    
    
    // фасад
    
    class profileFacade
    {
        private Person gamer;
        private ILocation location;

        
        public void setCharacter()
        {
            gamer = Person.getInstance();
            Console.WriteLine("Write your name (for example \"Denys\"): ");
            string name;
            name = Console.ReadLine();
            gamer.Name = name;
            Console.WriteLine("Good!\nThis is your own character \nwith his characteristics \nthat may change during the game.");
            Console.WriteLine($"Name: {gamer.Name}");
            int natureId = 0;
            Natures natures = new Natures();
            gamer.NatureID = natureId;
            Console.WriteLine($"Attentivness: {natures.natures[natureId].attentiveness}");
            Console.WriteLine($"Savvy: {natures.natures[natureId].savvy}");
            Console.WriteLine($"Speed: {natures.natures[natureId].speed}");
        }

        public void setLocation()
        {
            Console.WriteLine("We have several locations, choose one:");
            
            Console.WriteLine("1. Rural pond");
            Console.WriteLine("2. River bank");
            Console.WriteLine("Your choose: ");
            int i;
            i = Convert.ToInt32(Console.ReadLine());
            ILocation location = null;
            switch (i)
            {
                case 1 :
                    location = new RuralPond();
                    break;
                case 2 :
                    location = new RiverBank();
                    break;
            }
            
            Console.WriteLine($"Description: {location.getMap().description}");
            Console.WriteLine($"Weather: {location.getWeatherDescription()}");
            Console.WriteLine($"Your inventry: ");
            foreach (var item in location.getTackleCatalog())
            {
                Console.WriteLine(item.Name);
            }
            Console.WriteLine("Fish that you can catch:");
            foreach (var item in location.getFishList())
            {
                Console.WriteLine(item.name);
            }
            Console.WriteLine("Good luck!");
        }
        
    
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Fishing life!");
            profileFacade myProfile = new profileFacade();
            myProfile.setCharacter();
            myProfile.setLocation();
        }
    }
}
