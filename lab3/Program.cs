﻿// See https://aka.ms/new-console-template for more information

using System.Collections;
using System.Linq;
using System.Reflection.Metadata;
using Extension;
using Program;

namespace Extension
{
    static class MyExtensions
    {
        public static string toString(this Program.Nature nature)
        {
            return "id: " + nature.id.ToString() +
                   " speed:" + nature.speed.ToString() +
                   " attentiveness:" + nature.attentiveness.ToString() +
                   " savvy: " + nature.savvy.ToString();
        }
    }
}
namespace Program
{
    class Tackle
    {
            public string Name { get; set; }
            // public int ID { get; set; }
            public string Description { get; set; }
    }

    class Inventory
    {
        private List<Tackle> tackles;

        public Inventory()
        {
            tackles = new List<Tackle>();
        }

        public void addTackle(Tackle tackle)
        {
            this.tackles.Add(tackle);
        }

        public Tackle getTackleByIndex(int index)
        {
            return tackles[index];
        }

        public void removeTackle(Tackle tackle)
        {
            tackles.Remove(tackle);
        }

        public void removeTackleAt(int index)
        {
            tackles.RemoveAt(index);
        }
    }

    class Nature
    {
        public static int Low;
        public static int Normal;
        public static int High;

        static Nature()
        {
            Low = 1;
            Normal = 2;
            High = 3;
        }
        public int id { get; set; }
        public int speed { get; set; }
        public int attentiveness { get; set; }
        public int savvy { get; set; }

        public int summ()
        {
            return speed + savvy + attentiveness;
        }
    }

    
    
    class Person
    {
        public string Name { set; get; }
        public int NatureID { get; set; }
        public Inventory inventory;

        public Person(string name, int natureId)
        {
            this.Name = name;
            this.NatureID = natureId;
            this.inventory = new Inventory();
        }
        
    }

    class ModelTests
    {
        public List<Nature> natures
        {
            get
            {
                return new List<Nature>()
                {
                    new Nature()
                    {
                        id = 0,
                        speed = Nature.Low,
                        attentiveness = Nature.Low,
                        savvy = Nature.Low

                    },
                    
                    new Nature()
                    {
                        id = 1,
                        speed = Nature.Low,
                        attentiveness = Nature.Normal,
                        savvy = Nature.High

                    },
                    
                    new Nature()
                    {
                        id = 2,
                        speed = Nature.Normal,
                        attentiveness = Nature.Low,
                        savvy = Nature.Normal

                    }
                    
                    
                };
            }
        }

        public List<Person> users
        {
            get
            {
                return new List<Person>()
                {
                    new Person("Vasyl", 0),
                    new Person("Ivan", 1),
                    new Person("Oleg", 2),
                    new Person("Andriy", 2)
                };
            }
        }
    }

    class NatureComparer : IComparer<Nature>
    {
        public int Compare(Nature? p1, Nature? p2)
        {
            return p1.summ().CompareTo(p2.summ());
        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            ModelTests test = new ModelTests();
           
            
            Console.WriteLine("---Foreach---");
            foreach (var user in test.users)
            {
                Console.WriteLine(
                    "User: {0}, NatureID: {1}", user.Name, user.NatureID);
            }
            //-----------------------------------------------------------------

            var NaturesDictionary = new Dictionary<int, Nature>();
            NaturesDictionary.Add(2, test.natures[2]);
            NaturesDictionary.Add(0, test.natures[0]);
            NaturesDictionary.Add(1, test.natures[1]);
            

            Nature printNature;
            NaturesDictionary.TryGetValue(test.users[1].NatureID, out printNature);
            Console.WriteLine(
                $"{test.users[1].Name}: Speed - {printNature.speed}, Attentiveness - {printNature.attentiveness}, Savvy - {printNature.savvy}");

            SortedList<int, Nature> sortedList = new SortedList<int, Nature>();
            sortedList.Add(2, test.natures[2]);
            sortedList.Add(0, test.natures[0]);
            sortedList.Add(1, test.natures[1]);
            foreach (var item in sortedList)
            {
                Console.WriteLine(item.Key);
            }
            
            Console.WriteLine("********---------Group");
            //групування
            var groupOfUsers =
                from user in test.users
                group user by user.Name[0];

            foreach (var item in groupOfUsers)
            {
                Console.WriteLine(item.Key);
                foreach (var user in item)
                {
                    Console.WriteLine(user.Name);
                }
                Console.WriteLine("---------");
            }
            //where, select
            Console.WriteLine("********---------where, select");
            List<int> someList = Enumerable.Range(1, 10).ToList();

            IEnumerable<int> anotherList =
                from item in someList
                where item % 2 == 0
                select item * 2;

            foreach (var item in anotherList)
            {
                Console.WriteLine(item);
            }

            //compare
            Console.WriteLine("********---------compare");
            var natures = test.natures;
            var nc = new NatureComparer();
            natures.Sort(nc);

            foreach (var item in natures)
            {
               // Console.WriteLine($"id: {item.id}, speed:{item.speed}, attentiveness:{item.attentiveness}, savvy{item.savvy}");
               Console.WriteLine(item.toString());
            }
            
            //convert list to array
            Console.WriteLine("********---------convert list to array");
            Person[] array = test.users.ToArray();
            foreach (var item in array)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}